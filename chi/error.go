package japichi

import (
	"net/http"

	"github.com/go-chi/render"

	"keithcod.es/japi"
)

// Error response with an error
func Error(w http.ResponseWriter, r *http.Request, status int, title string, err error) {
	e := japi.Error{
		Status: string(status),
		Title:  title,
		Detail: err.Error(),
	}

	res := &japi.Response{
		Errors: []japi.Error{e},
	}

	render.Status(r, status)
	render.JSON(w, r, res)
}
