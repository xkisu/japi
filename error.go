package japi

// Error represents an error in the response
type Error struct {
	ID string `json:"id,omitempty"`
	// May contain an "about" field referencing info about the error
	Links Links `json:"links,omitempty"`
	// HTTP status code
	Status string `json:"status,omitempty"`
	// Application-specific error code
	Code string `json:"code,omitempty"`
	// Human-readable summary specific to the type of problem
	Title string `json:"title,omitempty"`
	// Human-readable description of the error
	Detail string      `json:"detail,omitempty"`
	Source interface{} `json:"source,omitempty"`
	Meta   interface{} `json:"meta,omitempty"`
}

// ToError returns a jsonapi error for the specified Golang error
func ToError(title string, err error) *Error {
	return &Error{
		Title:  title,
		Detail: err.Error(),
	}
}
