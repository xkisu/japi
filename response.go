package japi

// Response represents an api response
type Response struct {
	// Must
	Data   interface{} `json:"data,omitempty"`
	Errors []Error     `json:"errors,omitempty"`
	Meta   interface{} `json:"meta,omitempty"`
	// May
	JsonAPI  *JsonAPI      `json:"jsonapi,omitempty"`
	Links    Links         `json:"links,omitempty"`
	Included []interface{} `json:"included,omitempty"`
}
