# Japi

A Golang implementation of the jsonapi.org v1.0 standard.


# Chi

The `/chi` directory contains an implementation of Japi for using with the Golang Chi HTTP library.

# TODO

  * Compound documents
  * Validation