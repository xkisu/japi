package japi

// Relationships defines how a resource is related to other resources
type Relationships struct {
	Links Links       `json:"links"`
	Data  interface{} `json:"data"`
	Meta  interface{} `json:"meta,omitempty"`
}
