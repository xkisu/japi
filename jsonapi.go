package japi

// JsonAPI represents a jsonapi object
type JsonAPI struct {
	Version string      `json:"version"`
	Meta    interface{} `json:"meta,omitempty"`
}
