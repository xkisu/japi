package japi

// Resource represents a resource in the payload
type Resource struct {
	// Must
	ID   string `json:"id,omitempty"`
	Type string `json:"type"`
	// May
	// Object containing the attributes of the resource
	Attributes    interface{}              `json:"attributes,omitempty"`
	Relationships map[string]Relationships `json:"relationships,omitempty"`
	Links         Links                    `json:"links,omitempty"`
	Meta          interface{}              `json:"meta,omitempty"`
}
